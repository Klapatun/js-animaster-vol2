(function main() {

    /* FadeIn */
  document.getElementById("fadeInPlay").addEventListener("click", function () {
    const block = document.getElementById("fadeInBlock");
    animaster().fadeIn(block, 5000);
  });

  document.getElementById("fadeInStop").addEventListener("click", function () {
    const block = document.getElementById("fadeInBlock");
    resetfadeIn(block);
  });
    
    /* FadeOut */
  document.getElementById("fadeOutPlay").addEventListener("click", function () {
    const block = document.getElementById("fadeOutBlock");
    fadeOut(block, 5000);
  });
    
  document.getElementById("fadeOutStop").addEventListener("click", function () {
    const block = document.getElementById("fadeOutBlock");
    resetfadeOut(block);
  });
    
    /* move */
  document.getElementById("movePlay").addEventListener("click", function () {
    const block = document.getElementById("moveBlock");
    animaster().move(block, 1000, { x: 100, y: 10 });
  });
    
  document.getElementById("moveStop").addEventListener("click", function () {
    const block = document.getElementById("moveBlock");
    resetMove(block);
  });

    /* moveAndHide */
  document
    .getElementById("moveAndHidePlay")
    .addEventListener("click", function () {
      const block = document.getElementById("moveAndHideBlock");
      animaster().moveAndHide(block, 1000);
  });
    
  document.getElementById("moveAndHideStop").addEventListener("click", function () {
    const block = document.getElementById("moveAndHideBlock");
      resetMoveAndHide(block);
  });

    /* scale */
  document.getElementById("scalePlay").addEventListener("click", function () {
    const block = document.getElementById("scaleBlock");
    animaster().scale(block, 1000, 1.25);
  });
    
  document.getElementById("scaleStop").addEventListener("click", function () {
    const block = document.getElementById("scaleBlock");
    resetScale(block);
  });

    /* showAndHide */
  document
    .getElementById("showAndHidePlay")
    .addEventListener("click", function () {
      const block = document.getElementById("showAndHideBlock");
      animaster().showAndHide(block, 1000);
  });
    
  document
    .getElementById("showAndHideStop")
    .addEventListener("click", function () {
      const block = document.getElementById("showAndHideBlock");
    //   animaster().showAndHide(block, 1000);
    resetShowAndHide(block);
  });
    
})();

/**
 * Блок плавно появляется из прозрачного.
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 */
function fadeIn(element, duration) {
  element.style.transitionDuration = `${duration}ms`;
  element.classList.remove("hide");
  element.classList.add("show");
}
function resetfadeIn(element) {
  element.style.transition = "all";
  element.classList.remove("show");
  element.classList.add("hide");
}
/**
 * Функция, передвигающая элемент
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 * @param translation — объект с полями x и y, обозначающими смещение блока
 */
function move(element, duration, translation) {
  element.style.transitionDuration = `${duration}ms`;
  element.style.transform = getTransform(translation, null);
}
function resetMove(element) {
  element.style.transition = "all";
  element.style.transform = "none";
}

/**
 * Функция, увеличивающая/уменьшающая элемент
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
 */
function scale(element, duration, ratio) {
  element.style.transitionDuration = `${duration}ms`;
  element.style.transform = getTransform(null, ratio);
}

function resetScale(element) {
    element.style.transition = "all";
    element.style.transform = "none";
}

/**
 * Блок плавно исчезает.
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 */
function fadeOut(element, duration) {
  element.style.transitionDuration = `${duration}ms`;
  element.classList.remove("show");
  element.classList.add("hide");
}

function resetfadeOut(element) {
    element.style.transition = "all";
    element.classList.remove("hide");
    element.classList.add("show");
}

/**
 * Функция, показывающая элемент и через время скрывает его
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 */
function showAndHide(element, duration) {
  fadeIn(element, duration * (1 / 3));
  setTimeout(fadeOut, duration * (1 / 3), element, duration * (1 / 3));
}

function resetShowAndHide(element) {
    element.style.transition = "all";
    element.classList.remove("show");
    element.classList.add("hide");
}

/**
 * Функция, передвигающая элемент и скрывающая его
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 */
function moveAndHide(element, duration) {
  move(element, duration * (2 / 5), { x: 100, y: 20 });
  fadeOut(element, duration * (3 / 5));
}

function resetMoveAndHide(element) {
    element.style.transition = "all";
    element.style.transform = "none";
    element.classList.remove("hide");
    element.classList.add("show");
}


function getTransform(translation, ratio) {
  const result = [];
  if (translation) {
    result.push(`translate(${translation.x}px,${translation.y}px)`);
  }
  if (ratio) {
    result.push(`scale(${ratio})`);
  }
  return result.join(" ");
}

function animaster() {
  let obj = {
    scale: scale,
    fadeIn: fadeIn,

    move: move,

    moveAndHide: moveAndHide,

    fadeOut: fadeOut,

    showAndHide: showAndHide,
  };
  return obj;
}
